package com.hellodati.scanner

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import com.hellodati.scanner.barcode.BarcodeCaptureActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(applicationContext, BarcodeCaptureActivity::class.java)
        startActivityForResult(intent, BARCODE_READER_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    val barcode = data.getParcelableExtra<Barcode>(BarcodeCaptureActivity.BarcodeObject)
                    launchApp("com.hellodati.datiapp",barcode.displayValue)
                } else
                    Toast.makeText(applicationContext,R.string.no_barcode_captured,Toast.LENGTH_LONG).show()
            } else
                Toast.makeText(applicationContext,R.string.barcode_error_format,Toast.LENGTH_LONG).show()
                /*Log.e(LOG_TAG, String.format(getString(R.string.barcode_error_format),
                        CommonStatusCodes.getStatusCodeString(resultCode)))*/
        } else
            super.onActivityResult(requestCode, resultCode, data)
    }

    private fun launchApp(packageName: String,data: String) {
        // Get an instance of PackageManager
        val pm = applicationContext.packageManager

        // Initialize a new Intent
        val intent:Intent = pm.getLaunchIntentForPackage(packageName)

        // Add data to intent
        intent.putExtra("barcode",data)

        // Add category to intent
        intent.addCategory(Intent.CATEGORY_LAUNCHER)

        applicationContext.startActivity(intent)
        finish()

        /*// If intent is not null then launch the app
        if(intent!=null){
            applicationContext.startActivity(intent)
            finish()
        }*/
    }

    companion object {
        private val LOG_TAG = MainActivity::class.java.simpleName
        private val BARCODE_READER_REQUEST_CODE = 1
    }
}
